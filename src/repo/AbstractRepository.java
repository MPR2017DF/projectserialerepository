package repo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public abstract class AbstractRepository<TEntity> implements Repository<TEntity> {
	private Connection connection;

	public AbstractRepository(Connection connection){
		this.connection = connection;
		
		try {

			ResultSet rs = connection.getMetaData().getTables(null, null, null, null);

			boolean tableExists = false;
			while (rs.next()) {
				if (rs.getString("TABLE_NAME").equalsIgnoreCase(getTableName())) {
					tableExists = true;
					break;
				}
			}
			if (!tableExists) {
				Statement createTable = connection.createStatement();
				createTable.executeUpdate(getCreateTable());
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}
	protected abstract String getTableName();

	protected abstract String getCreateTable();
}
