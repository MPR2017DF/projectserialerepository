package repo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;  
import java.util.List;

import domain.Director;
import mappers.DirectorMapper;

public class DirectorRepository extends AbstractRepository<Director> {
	  private Connection connection;

	    private DirectorMapper directorMapper;

	    private String createDirectorTable = ""
	        + "CREATE TABLE Director("
	        + "id bigint GENERATED BY DEFAULT AS IDENTITY,"
	        + "name varchar(20),"
	        + "dateOfBirth date,"
	        + "biography varchar(30),"
	        + ")";

	    public DirectorRepository(Connection connection) {
	    	super(connection);

	        this.directorMapper = new DirectorMapper(connection);
	    }

	@Override
	public Director withId(int id) {
		return directorMapper.withId(id);	
	}

	@Override
	public List<Director> getAll() {
		return directorMapper.getAll();
	}

	@Override
	public void add(Director director) {
		directorMapper.add(director);
	}

	@Override
	public void modify(Director director) {
	directorMapper.modify(director);		
	}

	@Override
	public void remove(Director director) {
		directorMapper.delete(director);
		
	}

	@Override
	protected String getTableName() {
		return "Director";
	}

	@Override
	protected String getCreateTable() {
		return createDirectorTable;
	}

}
