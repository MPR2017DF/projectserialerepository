package test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;

import domain.Episode;
import service.EpisodeManager;

public class EpisodeManagerTest{

	EpisodeManager episodeManager = new EpisodeManager();
	
	private final static String NAME_1 = "NazwaOdcina";
	private final static Date ReleaseDate_1 = new GregorianCalendar(2014, Calendar.FEBRUARY, 11).getTime();
	private final static int EpisodeNumber_1 = 1;
	private final static int Duration_1 = 2;


	
	@Test
	public void checkConnection(){
		assertNotNull(episodeManager.getConnection());
	}
	
	@Test
	public void checkAdding(){
		
		Episode episode = new Episode( NAME_1, ReleaseDate_1, EpisodeNumber_1, Duration_1);
		
		episodeManager.clearEpisodes();
		assertEquals(1,episodeManager.addEpisode(episode, 0));
		
		List<Episode> episodes = episodeManager.getAllEpisodes();
		Episode personRetrieved = episodes.get(0);
		
		assertEquals(NAME_1, personRetrieved.getName());
		assertEquals(ReleaseDate_1, personRetrieved.getReleaseDate());
		assertEquals(EpisodeNumber_1, personRetrieved.getEpisodeNumber());
		assertEquals(Duration_1, personRetrieved.getDuration());
	}

}


