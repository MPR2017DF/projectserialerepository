package com.example.jdbcdemo.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.jdbcdemo.domain.Episode;

public class EpisodeMapper {
	private PreparedStatement addEpisodeStmt;
	private PreparedStatement deleteAllEpisodeStmt;
	private PreparedStatement getAllEpisodesStmt;
	private PreparedStatement selectBySeasonId;
	private Connection connection;

	public EpisodeMapper(Connection connection) {
		this.connection = connection;
		try {
			addEpisodeStmt = connection.prepareStatement(
					"INSERT INTO episode (name, releaseDate, episodeNumber, duration, seasonID) VALUES (?,?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			deleteAllEpisodeStmt = connection.prepareStatement("DELETE FROM Episode");
			getAllEpisodesStmt = connection
					.prepareStatement("SELECT id, name, releaseDate, episodeNumber, duration FROM Episode");
			selectBySeasonId = connection.prepareStatement("Select * from episode where seasonID=?");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	public Episode withId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Episode> getAll() {
		List<Episode> episodes = new ArrayList<Episode>();

		try {
			ResultSet rs = getAllEpisodesStmt.executeQuery();

			while (rs.next()) {
				Episode p = new Episode();
				p.setName(rs.getString("name"));
				p.setReleaseDate(rs.getDate("releaseDate"));
				p.setEpisodeNumber(rs.getInt("episodeNumber"));
				p.setDuration(rs.getInt("duration"));
				episodes.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return episodes;
	}

	public void add(Episode entity) {
		int count = 0;
		try {
			addEpisodeStmt.setString(1, episode.getName());
			addEpisodeStmt.setDate(2, (Date) episode.getReleaseDate());
			addEpisodeStmt.setInt(3, episode.getEpisodeNumber());
			addEpisodeStmt.setInt(4, episode.getDuration());
			addEpisodeStmt.setInt(5, seasonID);
			count = addEpisodeStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	public void modify(Episode entity) {
		// TODO Auto-generated method stub

	}

	public void remove(Episode entity) {
		try {
			deleteAllEpisodeStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public ArrayList<Episode> getBySeasonId(int seasonId) throws SQLException {
		selectBySeasonId.setInt(1, seasonId);
		ResultSet rs = selectBySeasonId.executeQuery();
		ArrayList<Episode> episodes = new ArrayList<Episode>();
		while (rs.next()) {
			Episode episode = getEpisode(rs);
			episodes.add(episode);
		}
		return episodes;
	}

	private Episode getEpisode(ResultSet rs) throws SQLException {
		Episode p = new Episode();
		p.setName(rs.getString("name"));
		p.setReleaseDate(rs.getDate("releaseDate"));
		p.setEpisodeNumber(rs.getInt("episodeNumber"));
		p.setDuration(rs.getInt("duration"));
		int episodeId = rs.getInt("id");
		p.setId(episodeId);
		return p;
	}
}
