package com.example.jdbcdemo.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.jdbcdemo.domain.Season;
import com.example.jdbcdemo.domain.TvSeries;

public class TvSeriesMapper {
	private PreparedStatement addTvSeriesStmt;
	private PreparedStatement deleteAllTvSeriesStmt;
	private PreparedStatement getAllTvSeriesStmt;

	private Connection connection;

	public TvSeriesMapper(Connection connection) {
		this.connection = connection;
		try {
			addTvSeriesStmt = connection.prepareStatement("Insert into TvSeries name values=?",
					Statement.RETURN_GENERATED_KEYS);
			deleteAllTvSeriesStmt = connection.prepareStatement("DELETE FROM TvSeries");
			getAllTvSeriesStmt = connection.prepareStatement("SELECT * FROM TvSeries");
			

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	public TvSeries withId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<TvSeries> getAll() {
		List<TvSeries> tvSerieses = new ArrayList<TvSeries>();

		try {
			ResultSet rs = getAllTvSeriessStmt.executeQuery();
			SeasonManager seasonManager = new SeasonManager();
			while (rs.next()) {
				TvSeries p = new TvSeries();
				int tvSeriesId = rs.getInt("id");
				p.setId(tvSeriesId);
				p.setName(rs.getString("name"));
				ArrayList<Season> seasons = seasonManager.getByTvSeriesId(tvSeriesId);
				p.setSeasons(seasons);
				tvSerieses.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tvSerieses;	}

	public void add(TvSeries entity) {
		int count = 0;
		try {
			addTvSeriesStmt.setString(1, TvSeries.getName());

			count = addTvSeriesStmt.executeUpdate();

			int tvSeriesID;

			ResultSet generatedKeys = addTvSeriesStmt.getGeneratedKeys();
			if (generatedKeys.next()) {
				tvSeriesID = generatedKeys.getInt(1);
				SeasonManager seasonManager = new SeasonManager();
				seasonManager.addSeasons(tvSeries.getSeasons(), tvSeriesID);
				DirectorManager directorManager = new DirectorManager();
				directorManager.addDirector(tvSeries.getDirector());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;

	}

	public void modify(TvSeries entity) {
		// TODO Auto-generated method stub

	}

	public void remove(TvSeries entity) {
		try {
			deleteAllTvSeriesStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
