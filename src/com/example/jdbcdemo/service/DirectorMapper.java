package com.example.jdbcdemo.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.jdbcdemo.domain.Director;

public class DirectorMapper {
	private PreparedStatement addDirectorStmt;
	private PreparedStatement deleteAllDirectorsStmt;
	private PreparedStatement getAllDirectorsStmt;
	private Connection connection;

	public DirectorMapper(Connection connection) {
		this.connection = connection;
		try {
			addDirectorStmt = connection
					.prepareStatement("INSERT INTO Director (name, dateOfBirth, biography) VALUES (?, ?, ?)");
			deleteAllDirectorsStmt = connection.prepareStatement("DELETE FROM Director");
			getAllDirectorsStmt = connection.prepareStatement("SELECT id, name, dateOfBirth, biography FROM Director");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Director withId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Director> getAll() {
		List<Director> Directors = new ArrayList<Director>();

		try {
			ResultSet rs = getAllDirectorsStmt.executeQuery();

			while (rs.next()) {
				Director p = new Director();
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("name"));
				p.setDateOfBirth(rs.getDate("dateOfBirth"));
				p.setBiography(rs.getString("biography"));
				Directors.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Directors;
	}

	public void add(Director Director) {
		int count = 0;
		try {
			addDirectorStmt.setString(1, Director.getName());
			addDirectorStmt.setDate(2, (Date) Director.getDateOfBirth());
			addDirectorStmt.setString(3, Director.getBiography());

			count = addDirectorStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	public void modify(Director entity) {
		// TODO Auto-generated method stub

	}

	public void remove(Director entity) {
		try {
			deleteAllDirectorsStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
