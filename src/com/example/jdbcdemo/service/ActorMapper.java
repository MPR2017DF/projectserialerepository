package com.example.jdbcdemo.service;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.jdbcdemo.domain.Actor;

public class ActorMapper {
	private PreparedStatement addActorStmt;
	private PreparedStatement deleteAllActorsStmt;
	private PreparedStatement getAllActorsStmt;
	private Connection connection;

	public ActorMapper(Connection connection) {
		this.connection = connection;
		try {
			addActorStmt = connection
					.prepareStatement("INSERT INTO Actor (name, dateOfBirth, biography) VALUES (?, ?, ?)");
			deleteAllActorsStmt = connection.prepareStatement("DELETE FROM Actor");
			getAllActorsStmt = connection.prepareStatement("SELECT id, name, dateOfBirth, biography FROM Actor");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	public Actor withId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Actor> getAll() {

		List<Actor> actors = new ArrayList<Actor>();

		try {
			ResultSet rs = getAllActorsStmt.executeQuery();

			while (rs.next()) {
				Actor p = new Actor();
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("name"));
				p.setDateOfBirth(rs.getDate("dateOfBirth"));
				p.setBiography(rs.getString("biography"));
				actors.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return actors;
	}

	public void add(Actor actor) {
		int count = 0;
		try {
			addActorStmt.setString(1, actor.getName());
			addActorStmt.setDate(2, (Date) actor.getDateOfBirth());
			addActorStmt.setString(3, actor.getBiography());

			count = addActorStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	public void modify(Actor entity) {
		// TODO Auto-generated method stub

	}

	public void remove(Actor entity) {
		try {
			deleteAllActorsStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
