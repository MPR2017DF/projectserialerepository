package com.example.jdbcdemo.service;

import java.util.List;

public interface Repository<TEntity> {

    TEntity withId(int id);
    List<TEntity> getAll();
    void add(TEntity entity);
    void modify(TEntity entity);
    void remove(TEntity entity);
}
