package com.example.jdbcdemo.service;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.example.jdbcdemo.domain.Episode;
import com.example.jdbcdemo.domain.Season;

public class SeasonMapper {
	private PreparedStatement addSeasonStmt;
	private PreparedStatement deleteAllSeasonStmt;
	private PreparedStatement getAllSeasonsStmt;
	private PreparedStatement selectByTvSeriesId;
	private Connection connection;

	public SeasonMapper(Connection connection) {
		this.connection = connection;
		try {
			addSeasonStmt = connection.prepareStatement(
					"INSERT INTO season (number, year_of_release, tvSeriesID) VALUES (?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			deleteAllSeasonStmt = connection.prepareStatement("DELETE FROM Season");
			getAllSeasonsStmt = connection.prepareStatement("SELECT id, seasonNumber, yearOfRlease FROM Season");
			selectByTvSeriesId = connection.prepareStatement("Select * from season where tvseriesid=?");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	public ArrayList<Season> getByTvSeriesId(int tvSeriesId) throws SQLException {
		selectByTvSeriesId.setInt(1, tvSeriesId);
		ResultSet rs = selectByTvSeriesId.executeQuery();
		ArrayList<Season> seasons = new ArrayList<Season>();

		while (rs.next()) {
			Season season = getSeason(rs);
			seasons.add(season);
		}
		return seasons;
	}

	private Season getSeason(ResultSet rs) throws SQLException {

		Season p = new Season();
		p.setSeasonNumber(rs.getInt("seasonNumber"));
		p.setYearOfRelease(rs.getInt("yearOfRlease"));
		int seasonId = rs.getInt("id");
		p.setId(seasonId);
		EpisodeManager episodeManager = new EpisodeManager();
		ArrayList<Episode> episodes = episodeManager.getBySeasonId(seasonId);
		p.setEpisodes(episodes);

		return p;
	}

	public Season withId(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Season> getAll() {
		List<Season> seasons = new ArrayList<Season>();

		try {
			ResultSet rs = getAllSeasonsStmt.executeQuery();

			while (rs.next()) {
				Season season = getSeason(rs);
				seasons.add(season);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return seasons;
	}

	public void add(ArrayList<Season> seasons, int tvSeriesID) {
		for (Season season : seasons) {
			addSeason(season, tvSeriesID);
		}
	}

	public void modify(Season entity) {
		// TODO Auto-generated method stub

	}

	public void remove(Season entity) {
		try {
			deleteAllSeasonStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}