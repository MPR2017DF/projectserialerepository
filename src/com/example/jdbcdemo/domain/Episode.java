package com.example.jdbcdemo.domain;



import java.util.Date;

public class Episode {

	private long id;
	private String name;
	private Date releaseDate;
	private int episodeNumber;
	private int duration;

	
	public Episode() {
	}
	
	public Episode( String name, Date releaseDate, int episodeNumber, int duration ) {
		super();
		this.name = name;
		this.releaseDate = releaseDate;
		this.episodeNumber = episodeNumber;
		this.duration = duration;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getEpisodeNumber() {
		return episodeNumber;
	}
	public void setEpisodeNumber(int episodeNumber) {
		this.episodeNumber = episodeNumber;
	}
	public int getDuration() {
		return duration;
	}
	public void setDuration(int duration ) {
		this.duration = duration;
	}
}
