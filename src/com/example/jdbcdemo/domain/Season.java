package com.example.jdbcdemo.domain;


import java.util.ArrayList;

public class Season {

	private long id;
	private int seasonNumber;
	private int yearOfRelease;
	private ArrayList<Episode> episodes;
	
	public Season() {
	}
	
	public Season(int seasonNumber, int yearOfRlease, ArrayList<Episode> episodes){
		super();
		this.seasonNumber = seasonNumber;
		this.yearOfRelease = yearOfRlease;
	}

	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public int getSeasonNumber() {
		return seasonNumber;
	}
	public void setSeasonNumber(int seasonNumber) {
		this.seasonNumber = seasonNumber;
	}
	public int getYearOfRelease() {
		return yearOfRelease;
	}
	public void setYearOfRelease(int yearOfRlease) {
		this.yearOfRelease = yearOfRlease;
	}
	public ArrayList<Episode> getEpisodes() {
		return episodes;
	}

	public void setEpisodes(ArrayList<Episode> episodes) {
		this.episodes = episodes;
	}

}
