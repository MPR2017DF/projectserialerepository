package com.example.jdbcdemo.domain;

import java.util.Date;

public class Director {
	private long id;
	
	private String name;
	private Date dateOfBirth;
	private String biography;
	
	public Director(){
	}
	
	public Director(String name, Date dateOfBirth, String biography) {
		super();
		this.name = name;
		this.dateOfBirth = dateOfBirth;
		this.biography = biography;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getBiography() {
		return biography;
	}
	public void setBiography(String biography) {
		this.biography = biography;
	}
}
