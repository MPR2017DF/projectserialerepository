package mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Episode;
import domain.Season;
import service.EpisodeManager;

public class SeasonMapper {
	private PreparedStatement addSeasonStmt;
	private PreparedStatement deleteAllSeasonStmt;
	private PreparedStatement getAllSeasonsStmt;
	private PreparedStatement selectByTvSeriesId;
	private PreparedStatement selecById;
	private PreparedStatement modifySeason;
	private PreparedStatement deleteSeason; 
	private Connection connection;

	public SeasonMapper(Connection connection) {
		this.connection = connection;
		try {
			addSeasonStmt = connection.prepareStatement(
					"INSERT INTO season (number, year_of_release, tvSeriesID) VALUES (?,?,?)",
					Statement.RETURN_GENERATED_KEYS);
			deleteAllSeasonStmt = connection.prepareStatement("DELETE FROM Season");
			getAllSeasonsStmt = connection.prepareStatement("SELECT id, seasonNumber, yearOfRlease FROM Season");
			selectByTvSeriesId = connection.prepareStatement("Select * from season where tvseriesid=?");
			selecById = connection.prepareStatement("Select * from Season where Id = ?");
			deleteSeason = connection.prepareStatement("DELETE FROM Season where id = ? ");
			modifySeason = connection
					.prepareStatement(" UPDATE season set number = ?, year_of_release = ?  " + " where id = ? ");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	public ArrayList<Season> getByTvSeriesId(long tvSeriesId) throws SQLException {
		selectByTvSeriesId.setLong(1, tvSeriesId);
		ResultSet rs = selectByTvSeriesId.executeQuery();
		ArrayList<Season> seasons = new ArrayList<Season>();

		while (rs.next()) {
			Season season = getSeason(rs);
			seasons.add(season);
		}
		return seasons;
	}

	private Season getSeason(ResultSet rs) throws SQLException {

		Season p = new Season();
		p.setSeasonNumber(rs.getInt("seasonNumber"));
		p.setYearOfRelease(rs.getInt("yearOfRlease"));
		int seasonId = rs.getInt("id");
		p.setId(seasonId);
		EpisodeManager episodeManager = new EpisodeManager();
		ArrayList<Episode> episodes = episodeManager.getBySeasonId(seasonId);
		p.setEpisodes(episodes);

		return p;
	}

	public List<Season> getAll() {
		List<Season> seasons = new ArrayList<Season>();

		try {
			ResultSet rs = getAllSeasonsStmt.executeQuery();

			while (rs.next()) {
				Season season = getSeason(rs);
				seasons.add(season);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return seasons;
	}

	public void addSeasons(ArrayList<Season> seasons, int tvSeriesID) {
		for (Season season : seasons) {
			addSeason(season, tvSeriesID);
		}
	}

	private void addSeason(Season season, int tvSeriesID) {

		try {
			addSeasonStmt.setInt(1, season.getSeasonNumber());
			addSeasonStmt.setInt(2, season.getYearOfRelease());
			addSeasonStmt.setInt(3, tvSeriesID);
			addSeasonStmt.executeUpdate();

			int seasonID;

			ResultSet generatedKeys = addSeasonStmt.getGeneratedKeys();
			if (generatedKeys.next()) {
				seasonID = generatedKeys.getInt(1);
				EpisodeManager episodeManager = new EpisodeManager();
				episodeManager.addEpisode(season.getEpisodes(), seasonID);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return;
	}

	public void addSeason(Season season) {

		try {
			addSeasonStmt.setInt(1, season.getSeasonNumber());
			addSeasonStmt.setInt(2, season.getYearOfRelease());
			addSeasonStmt.setInt(3, (Integer) null);
			addSeasonStmt.executeUpdate();

			int seasonID;

			ResultSet generatedKeys = addSeasonStmt.getGeneratedKeys();
			if (generatedKeys.next()) {
				seasonID = generatedKeys.getInt(1);
				EpisodeManager episodeManager = new EpisodeManager();
				episodeManager.addEpisode(season.getEpisodes(), seasonID);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return;
	}

	public void modify(Season season) {
		try {
			modifySeason.setInt(1, season.getSeasonNumber());
			modifySeason.setInt(2, season.getYearOfRelease());
			modifySeason.setLong(3, season.getId());
			modifySeason.executeUpdate();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}

	public void remove(Season season) {
		try {
			modifySeason.setLong(1, season.getId());
			deleteSeason.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Season findById(int id) {
		try {
			selecById.setInt(1, id);
			Season season = new Season();
			ResultSet rs = selecById.executeQuery();
			season = getSeason(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

}