package mappers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Season;
import domain.TvSeries;
import service.DirectorManager;
import service.SeasonManager;

public class TvSeriesMapper {
	private PreparedStatement addTvSeriesStmt;
	private PreparedStatement deleteAllTvSeriesStmt;
	private PreparedStatement getAllTvSeriesStmt;
	private PreparedStatement selecById;
	private PreparedStatement modifyTvSeries;
	private PreparedStatement deleteTvSeries;

	private Connection connection;

	public TvSeriesMapper(Connection connection) {
		this.connection = connection;
		try {
			addTvSeriesStmt = connection.prepareStatement("Insert into TvSeries name values=?",
					Statement.RETURN_GENERATED_KEYS);
			deleteAllTvSeriesStmt = connection.prepareStatement("DELETE FROM TvSeries");
			getAllTvSeriesStmt = connection.prepareStatement("SELECT * FROM TvSeries");
			selecById = connection.prepareStatement("Select * from TvSeries where Id = ?");
			modifyTvSeries = connection
					.prepareStatement(" UPDATE tvseries set name = ? where id = ? ");
			deleteTvSeries = connection.prepareStatement("DELETE FROM TvSeries where id = ? ");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	public TvSeries withId(int id) {
		TvSeries tvSeries = new TvSeries();
		try {
			selecById.setInt(1, id);
			ResultSet rs = selecById.executeQuery();
			tvSeries = getTvSeries(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tvSeries;
	}

	private TvSeries getTvSeries(ResultSet rs) throws SQLException {
		SeasonManager seasonManager = new SeasonManager();
		TvSeries p = new TvSeries();
		int tvSeriesId = rs.getInt("id");
		p.setId(tvSeriesId);
		p.setName(rs.getString("name"));
		return p;
	}

	public List<TvSeries> getAll() {
		List<TvSeries> tvSerieses = new ArrayList<TvSeries>();

		try {
			ResultSet rs = getAllTvSeriesStmt.executeQuery();
			SeasonMapper seasonMapper = new SeasonMapper(connection);
			while (rs.next()) {
				TvSeries p = getTvSeries(rs);
				ArrayList<Season> seasons = seasonMapper.getByTvSeriesId(p.getId());
				p.setSeasons(seasons);
				tvSerieses.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return tvSerieses;
	}

	public void add(TvSeries tvSeries) {
		try {
			addTvSeriesStmt.setString(1, tvSeries.getName());

			addTvSeriesStmt.executeUpdate();

			int tvSeriesID;

			ResultSet generatedKeys = addTvSeriesStmt.getGeneratedKeys();
			if (generatedKeys.next()) {
				tvSeriesID = generatedKeys.getInt(1);
				SeasonManager seasonManager = new SeasonManager();
				seasonManager.addSeasons(tvSeries.getSeasons(), tvSeriesID);
				DirectorManager directorManager = new DirectorManager();
				directorManager.addDirector(tvSeries.getDirector());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void modify(TvSeries tvSeries) {
		try {
			modifyTvSeries.setString(1, tvSeries.getName());
			modifyTvSeries.setLong(2, tvSeries.getId());
			modifyTvSeries.executeUpdate();
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
	}

	public void remove(TvSeries tvSeries) {
		try {
			deleteAllTvSeriesStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void delete(TvSeries tvSeries) {
		try {
			deleteTvSeries.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
