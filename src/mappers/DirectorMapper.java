package mappers;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Director;
import domain.Episode;

public class DirectorMapper {
	private PreparedStatement addDirectorStmt;
	private PreparedStatement deleteAllDirectorsStmt;
	private PreparedStatement getAllDirectorsStmt;
	private PreparedStatement selecById;
	private PreparedStatement modifyDirectors;
	private PreparedStatement deleteDirector;

	public DirectorMapper(Connection connection) {
		try {
			addDirectorStmt = connection
					.prepareStatement("INSERT INTO Director (name, dateOfBirth, biography) VALUES (?, ?, ?)");
			deleteAllDirectorsStmt = connection.prepareStatement("DELETE FROM Director");
			getAllDirectorsStmt = connection.prepareStatement("SELECT id, name, dateOfBirth, biography FROM Director");
			selecById = connection.prepareStatement("Select * from director where Id = ?");
			modifyDirectors = connection.prepareStatement(
					" UPDATE director set name = ?,dateOfBirth = ?, biography = ? where id = ? ");
			deleteDirector = connection.prepareStatement(" DELETE FROM director where id = ? ");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Director withId(int id) {
		Director director = new Director();
		try {
			selecById.setInt(1, id);
			ResultSet rs = selecById.executeQuery();
			director = getDirector(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return director;
	}

	private Director getDirector(ResultSet rs) throws SQLException {
		Director p = new Director();
		p.setId(rs.getInt("id"));
		p.setName(rs.getString("name"));
		p.setDateOfBirth(rs.getDate("dateOfBirth"));
		p.setBiography(rs.getString("biography"));
		return p;
	}

	public List<Director> getAll() {
		List<Director> Directors = new ArrayList<Director>();

		try {
			ResultSet rs = getAllDirectorsStmt.executeQuery();

			while (rs.next()) {
				Director p = new Director();
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("name"));
				p.setDateOfBirth(rs.getDate("dateOfBirth"));
				p.setBiography(rs.getString("biography"));
				Directors.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return Directors;
	}

	public int add(Director director) {
		int count = 0;
		try {
			addDirectorStmt.setString(1, director.getName());
			addDirectorStmt.setDate(2, (Date) director.getDateOfBirth());
			addDirectorStmt.setString(3, director.getBiography());

			count = addDirectorStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	public void modify(Director director) {
		try {
			modifyDirectors.setString(1, director.getName());
			modifyDirectors.setDate(2, (Date) director.getDateOfBirth());
			modifyDirectors.setString(3, director.getBiography());
			modifyDirectors.setLong(4, director.getId());
		} catch (SQLException e) {
			
			e.printStackTrace();
		}

	}

	public void remove(Director directro) {
		try {
			deleteAllDirectorsStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void delete(Director director) {
		try {
			deleteDirector.setLong(1, director.getId());
			deleteDirector.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
