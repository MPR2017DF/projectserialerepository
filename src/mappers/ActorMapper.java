package mappers;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import domain.Actor;
import domain.Director;

public class ActorMapper {
	private PreparedStatement addActorStmt;
	private PreparedStatement deleteAllActorsStmt;
	private PreparedStatement getAllActorsStmt;
	private PreparedStatement selecById;
	private PreparedStatement modifyActors;
	private PreparedStatement deleteActor;
	private Connection connection;

	public ActorMapper(Connection connection) {
		this.connection = connection;
		try {
			addActorStmt = connection
					.prepareStatement("INSERT INTO Actor (name, dateOfBirth, biography) VALUES (?, ?, ?)");
			deleteAllActorsStmt = connection.prepareStatement("DELETE FROM Actor");
			getAllActorsStmt = connection.prepareStatement("SELECT id, name, dateOfBirth, biography FROM Actor");
			selecById = connection.prepareStatement("Select * from actor where Id = ?");
			modifyActors = connection.prepareStatement(
					" UPDATE actor set name = ?,dateOfBirth = ?, biography = ? where id = ? ");
			deleteActor = connection.prepareStatement(" DELETE FROM actor where id = ? ");

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	Connection getConnection() {
		return connection;
	}

	public Actor withId(int id) {
		Actor actor = new Actor();
		try {
			selecById.setInt(1, id);
			ResultSet rs = selecById.executeQuery();
			actor = getActor(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return actor;
	}

	private Actor getActor(ResultSet rs) throws SQLException {
		Actor p = new Actor();
		p.setId(rs.getInt("id"));
		p.setName(rs.getString("name"));
		p.setDateOfBirth(rs.getDate("dateOfBirth"));
		p.setBiography(rs.getString("biography"));
		return p;
	}

	public List<Actor> getAll() {

		List<Actor> actors = new ArrayList<Actor>();

		try {
			ResultSet rs = getAllActorsStmt.executeQuery();

			while (rs.next()) {
				Actor p = new Actor();
				p.setId(rs.getInt("id"));
				p.setName(rs.getString("name"));
				p.setDateOfBirth(rs.getDate("dateOfBirth"));
				p.setBiography(rs.getString("biography"));
				actors.add(p);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return actors;
	}

	public int add(Actor actor) {
		int count = 0;
		try {
			addActorStmt.setString(1, actor.getName());
			addActorStmt.setDate(2, (Date) actor.getDateOfBirth());
			addActorStmt.setString(3, actor.getBiography());

			count = addActorStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return count;
	}

	public void modify(Actor actor) {		
		try {
			modifyActors.setString(1, actor.getName());
			modifyActors.setDate(2, (Date) actor.getDateOfBirth());
			modifyActors.setString(3, actor.getBiography());
			modifyActors.setLong(4, actor.getId());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void remove(Actor actor) {
		try {
			deleteAllActorsStmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void delete(Actor actor) {
		try {
			deleteActor.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
