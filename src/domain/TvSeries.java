package domain;

import java.util.ArrayList;

public class TvSeries {

	private long id;

	private String name;
	private ArrayList<Season> seasons;
	private Director director;
	private ArrayList<Actor> actors;

	public TvSeries() {
	}

	public TvSeries(String name, ArrayList<Season> seasons) {
		super();
		this.name = name;
		this.seasons = seasons;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Season> getSeasons() {
		return seasons;
	}

	public void setSeasons(ArrayList<Season> seasons) {
		this.seasons = seasons;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	public ArrayList<Actor> getActors() {
		return actors;
	}

	public void setActors(ArrayList<Actor> actors) {
		this.actors = actors;
	}
}
